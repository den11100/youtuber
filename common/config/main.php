<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru',
    'components' => [
        'formatter' => [
            'dateFormat' => 'dd-MM-Y',
            'datetimeFormat' => 'd-MM-Y H:i:s',
            'timeFormat' => 'H:i:s',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

    ],
];
