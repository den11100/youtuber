<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $created
 * @property int $user_id
 * @property string $url
 * @property string $description
 * @property int $second
 * @property int $budget
 * @property int $price
 * @property int $quantity
 * @property int $order_quantity
 * @property int $type
 * @property int $status
 * @property int $rang
 *
 * @property User $user
 */
class Task extends \yii\db\ActiveRecord
{
    const TYPE_WATCH = 1;
    const TYPE_SUBSCRIBE = 2;

    const STATUS_WORK = 1;
    const STATUS_PAUSE = 2;
    const STATUS_FINISH = 3;

    const WITH_CURRENT_USER = 1;
    const WITHOUT_CURRENT_USER = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created'], 'safe'],

            [['user_id', 'second', 'budget', 'price', 'type', 'status', 'url', 'description'], 'required'],

            [['budget', 'price', 'second', 'user_id', 'type', 'status', 'rang'], 'integer', 'min' => 1],

            [['quantity', 'order_quantity'], 'integer', 'min' => 0,],

            ['price', 'compare', 'compareAttribute' => 'budget', 'operator' => '<=', 'type' => 'number'],

            [['url', 'description'], 'string', 'max' => 255],

            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function getStatus()
    {
        return ArrayHelper::getValue([1 => 'В работе', 2 => 'Пауза', 3 => 'Выполнен'], $this->status);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created' => 'Created',
            'user_id' => 'Пользователь',
            'url' => 'Url',
            'description' => 'Название',
            'second' => 'Секунд',
            'budget' => 'Бюджет',
            'price' => 'Цена за 1 просмотр',
            'quantity' => 'Количество просмотров',
            'type' => 'Тип задания',
            'status' => 'Статус',
            'rang' => 'Ранжирование',
            'order_quantity' => 'Всего',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return mixed
     */
    public function getRateTask()
    {
        return ArrayHelper::getValue(Yii::$app->params['rate'],$this->second);
    }

    /**
     * @param int $isCurrentUserInclude
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getRandomFilm($isCurrentUserInclude)
    {
        if ($isCurrentUserInclude === 1) {
            $taskListArray = Task::find()
                ->where(['status' => Task::STATUS_WORK])
                ->andWhere(['not in', 'user_id', Yii::$app->params['users-not-random']])
                ->asArray()
                ->all();
        } elseif ($isCurrentUserInclude === 0) {
            $taskListArray = Task::find()
                ->where(['status' => Task::STATUS_WORK])
                ->andWhere(['not in', 'user_id', Yii::$app->params['users-not-random']])
                ->andWhere(['not in', 'user_id', [Yii::$app->user->getId()]])
                ->asArray()
                ->all();
        }

        /* Если нет заданий */
        if ($taskListArray == []) {
            return null;
        }

        $taskIds = ArrayHelper::getColumn($taskListArray, 'id');
        $randomKey = array_rand($taskIds,1);
        $randomIdTask = $taskIds[$randomKey];

        return Task::find()->where(['id' => $randomIdTask])->asArray()->one();
    }
}
