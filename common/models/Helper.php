<?php

namespace common\models;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

class Helper extends \yii\base\Model
{

    /**
     * @param $string
     * @return string
     */
    public static function cleanData($string)
    {
        $string = str_replace("'", "", $string);
        $string = str_replace("\"", "", $string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string);
        $string = addslashes($string);

        $quotes = array ("\x27", "\x22", "\x60", "\t", "\n", "\r", "*", "%", "<", ">", "?", "!" , "«", "»");
        $string = str_replace( $quotes, '', $string );

        return $string;
    }

    /**
     * Проверка на доступ к своим таскам
     * @param string $nameClass
     * @param integer $id
     * @return bool
     * @throws ForbiddenHttpException
     */
    public static function checkWhoIsAuthor($nameClass, $id)
    {
        $model = $nameClass::findOne($id);

        if ($model) {
            if ($model->user_id == Yii::$app->user->identity->getId()) {
                return true;
            } else {
                throw new ForbiddenHttpException('Access denied');
            }
        }
        return true;
    }

    /**
     * @param $id integer
     * @return string
     */
    public static function getLabelmethodPay($id)
    {
        return ArrayHelper::getValue(Yii::$app->params['method-pay'], $id);
    }

    /**
     * @param $id integer
     * @return string
     */
    public static function getValueMethodForYandexMoney($id)
    {
        return ArrayHelper::getValue(Yii::$app->params['method-for-yandex-form'], $id);
    }

}
