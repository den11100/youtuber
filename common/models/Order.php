<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * $created - datetime create in db automatic current_timestamp
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $created
 * @property int $user_id
 * @property int $status
 * @property string $date_paid
 * @property int $method
 * @property int $amount
 * @property string $operation_number
 * @property string $info
 * @property string $yandex_wallet
 *
 * @property User $user
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    const METHOD_PAY_WEBMONEY = 3;

    const ORDER_STATUS_NEW = 1;
    const ORDER_STATUS_PAID = 2;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created', 'date_paid'], 'safe'],
            [['operation_number', 'info', 'yandex_wallet'], 'string', 'max' => 255],
            [['user_id', 'status', 'method', 'amount'], 'required'],
            [['user_id', 'status', 'method', 'amount'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created' => 'Created',
            'user_id' => 'User ID',
            'status' => 'Status',
            'date_paid' => 'Date Paid',
            'method' => 'Method',
            'operation_number' => 'Operation Number',
            'info' => 'Info',
            'yandex_wallet' => 'Yandex Wallet',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->status == self::ORDER_STATUS_PAID) {
                $this->date_paid = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getNameMethod()
    {
        return ArrayHelper::getValue(Yii::$app->params['label-method-pay'], $this->method);
    }

    /**
     * @return false|string
     * @throws \yii\base\InvalidConfigException
     */
    public function createdMoscowTimeZone()
    {
        $date = Yii::$app->formatter->asDatetime($this->created);
        return date('d-m-Y H:i:s', strtotime("+3 hours", strtotime($date)));
    }

    /**
     * @return false|string
     * @throws \yii\base\InvalidConfigException
     */
    public function createdThaiTimeZone()
    {
        $date = Yii::$app->formatter->asDatetime($this->created);
        return date('d-m-Y H:i:s', strtotime("+7 hours", strtotime($date)));
    }

    /**
     * @return false|string
     * @throws \yii\base\InvalidConfigException
     */
    public function datePaidMoscowTimeZone()
    {
        $date = Yii::$app->formatter->asDatetime($this->date_paid);
        return date('d-m-Y H:i:s', strtotime("+3 hours", strtotime($date)));
    }

    /**
     * @return false|string
     * @throws \yii\base\InvalidConfigException
     */
    public function datePaidThaiTimeZone()
    {
        $date = Yii::$app->formatter->asDatetime($this->date_paid);
        return date('d-m-Y H:i:s', strtotime("+7 hours", strtotime($date)));
    }
}
