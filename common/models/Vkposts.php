<?php

namespace common\models;

use VK\Client\VKApiClient;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "vkposts".
 *
 * @property int $id
 * @property string $vk_id_post
 * @property string $vk_id_user
 * @property string $vk_id_wall
 * @property string $text
 * @property int $date
 * @property string $platform
 * @property string $link_video
 * @property int $duration_video
 * @property int $watched
 */
class Vkposts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vkposts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vk_id_post', 'vk_id_user', 'vk_id_wall', 'text', 'date'], 'required'],
            [['text'], 'string'],
            [['date', 'duration_video'], 'integer'],
            [['vk_id_post', 'vk_id_user', 'vk_id_wall', 'platform', 'link_video'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vk_id_post' => 'Vk Id Post',
            'vk_id_user' => 'Vk Id User',
            'vk_id_wall' => 'Vk Id Wall',
            'text' => 'Text',
            'date' => 'Date',
            'platform' => 'Platform',
            'link_video' => 'Link Video',
            'duration_video' => 'Duration Video',
        ];
    }

    /**
     * @param array $posts
     */
    public static function savePosts($posts)
    {
        foreach ($posts['items'] as $post) {
            $vkPost = new Vkposts();
            $vkPost->vk_id_post = "" . $post['id'];
            $vkPost->vk_id_user = "" . $post['from_id'];
            $vkPost->vk_id_wall = substr($post['owner_id'],1);
            $vkPost->text = $post['text'];
            $vkPost->date = $post['date'];
            $vkPost->grabVideoUrlFromText();
            $vkPost->getVideoInfo($post);

            if ($vkPost->checkUserAndVideoBeforeSave()) {
                $vkPost->save();
            }
        }
    }

    /**
     * separate url from text
     */
    private function grabVideoUrlFromText()
    {
        if ($this->text) {

            $arrayWords = explode(' ', $this->text);

            foreach ($arrayWords as $item)
            {
                $linkAlter = stristr(trim($item), 'https://youtu.be/');
                $link = stristr(trim($item), 'https://www.youtube.com/watch');

                if ($linkAlter) {
                    preg_match('#(https:\/\/youtu.be\/.{11})#su', $linkAlter, $matches);
                    $this->link_video = $matches[0];
                } elseif ($link) {
                    preg_match('#(https:\/\/www.youtube.com\/.{19})#su', $link, $matches);
                    $this->link_video = $matches[0];
                }
            }
        }
    }

    /**
     * @param array $item
     */
    private function getVideoInfo($item)
    {
        if (isset($item['attachments'])) {

            foreach ($item['attachments'] as $infoVideo) {

                if ($infoVideo['type'] == 'video') {
                    $this->duration_video = $infoVideo['video']['duration'];
                }

            }

        }
    }

    /**
     * @return bool
     */
    private function checkUserAndVideoBeforeSave()
    {
        if ($this->link_video === null) {
            return false;
        }

        $post = self::find()->where(['vk_id_post' => $this->vk_id_post])->asArray()->one();
        if ($post) {
            return false;
        }

        $duplicateUrl = self::find()->where(['link_video' => $this->link_video])->asArray()->one();
        if ($duplicateUrl) {
            return false;
        }

        $user = $this->getInfoUser();
        if ($user[0]['can_write_private_message'] == 0) {
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    private function getInfoUser()
    {
        $vk = new VKApiClient();
        $user = $vk->users()->get(Yii::$app->params['vk_access_token'], [
            'user_ids' => $this->vk_id_user,
            'fields' => 'can_write_private_message'
        ]);
        return $user;
    }

    /**
     * @param $vkPost \backend\models\Vkposts
     * @throws \VK\Exceptions\Api\VKApiMessagesChatBotFeatureException
     * @throws \VK\Exceptions\Api\VKApiMessagesChatUserNoAccessException
     * @throws \VK\Exceptions\Api\VKApiMessagesDenySendException
     * @throws \VK\Exceptions\Api\VKApiMessagesForwardAmountExceededException
     * @throws \VK\Exceptions\Api\VKApiMessagesForwardException
     * @throws \VK\Exceptions\Api\VKApiMessagesKeyboardInvalidException
     * @throws \VK\Exceptions\Api\VKApiMessagesPrivacyException
     * @throws \VK\Exceptions\Api\VKApiMessagesTooLongMessageException
     * @throws \VK\Exceptions\Api\VKApiMessagesUserBlockedException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public static function sendScreen($vkPost)
    {
        $vk = new VKApiClient();

        $photo = self::addPhoto($vkPost, $vk);

        $result = $vk->messages()->send(Yii::$app->params['vk_access_token'], [
            'user_id' => $vkPost['vk_id_user'],
            'message' => self::getTextMessage(),
            'attachment' => 'photo' . $photo[0]['owner_id'] . '_' . $photo[0]['id']
        ]);

        return $result;
    }

    public static function addPhoto($vkPost, $vk)
    {
        // VK API-Урок загрузка фото в альбом группы через PHP и CURL
        $image_path = Yii::getAlias("@app") . "/tests/_output/debug/". $vkPost['vk_id_post'] . '_' . $vkPost['vk_id_wall'] . '.png'; //путь до картинки

        $url = $vk->photos()->getMessagesUploadServer(Yii::$app->params['vk_access_token'], [
            'peer_id' => $vkPost['vk_id_user']
        ]);

        // отправка post картинки
        $ch = curl_init();
        $file = curl_file_create($image_path, mime_content_type($image_path), pathinfo($image_path)['basename']);
        curl_setopt($ch, CURLOPT_URL, $url['upload_url']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['file' => $file]);
        $result = json_decode(curl_exec($ch),true);
        curl_close($ch);
        //print_r($result);die;

        $resultSave = $vk->photos()->saveMessagesPhoto(Yii::$app->params['vk_access_token'], [
           'photo' => $result['photo'],
           'server' => $result['server'],
           'hash' => $result['hash']
        ]);
        return $resultSave;
    }

    /**
     * @return string
     */
    public static function getRandomYoutubeComment()
    {
        $randomIndex = array_rand(Yii::$app->params['youtube-comments']);
        return Yii::$app->params['youtube-comments'][$randomIndex];
    }

    private static function getTextMessage()
    {
        $randomIndex = array_rand(Yii::$app->params['my-vk-message']);
        return Yii::$app->params['my-vk-message'][$randomIndex];
    }
}
