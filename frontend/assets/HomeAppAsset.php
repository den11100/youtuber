<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class HomeAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/creative.css',
        'css/site.css',
    ];
    public $js = [
        'js/bootstrap.js',
        'js/bootstrap.bundle.min.js',
        'js/jquery.easing.min.js',
        'js/scrollreveal.min.js',
        'js/creative.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}

