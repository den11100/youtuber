<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome.min.css',
        'css/ionicons.min.css',
        'css/AdminLTE.css',
        'css/skin-purple-light.css',
        'css/site.css',
    ];
    public $js = [
        'js/fastclick.js',
        'js/adminlte.min.js',
        'js/jquery.sparkline.min.js',
        'js/jquery.slimscroll.min.js',
        //'js/dashboard2.js',
        'js/cabinet.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
