<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Task */
/* @var int $averagePrice */

$this->title = 'Добавить задание - просмотр видео';
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="alert alert-warning alert-dismissible">
    <p><i class="icon fa fa-warning"></i> Будьте внимательны задание нельзя отменить или поправить.</p>
</div>

<?= $this->render('_form', [
    'model' => $model,
    'averagePrice' => $averagePrice,
]) ?>

<?php $this->registerJsFile('@web/js/create-task.js', [
        'depends' => \yii\web\JqueryAsset::className(),
]);
