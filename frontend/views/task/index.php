<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Задания';
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить задание', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="alert alert-warning alert-dismissible">
        <p><i class="icon fa fa-warning"></i> При удалении заданий, поинты не возвращаются</p>
    </div>
    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'description',
                    'value' => function($data) {
                        return "<a target='_blank' href='$data->url'>" .$data->description. "</a>";
                    },
                    'format' => 'raw',
                ],
                'second',
                [
                    'attribute' => 'budget',
                    'value' => function($data) {
                        return $data->budget . " поинт";
                    }
                ],
                [
                    'attribute' => 'price',
                    'value' => function($data) {
                        return $data->price . " поинт";
                    }
                ],
                [
                    'attribute' => 'order_quantity',
                    'label' => 'Выполнено',
                    'value' => function($data) {
                        return $data->quantity . ' / '. $data->order_quantity ;
                    }
                ],
                [
                    'attribute' => 'status',
                    'value' => function($data) {
                        return $data->getStatus();
                    },
                ],
                'rang',

                ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}'],
            ],
        ]); ?>
    </div>
</div>
