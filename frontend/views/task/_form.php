<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Task */
/* @var $form yii\widgets\ActiveForm */
/* @var int $averagePrice */
?>

<div class="task-form">

    <div class="row">

        <div class="col-md-6">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'url')->textInput(['maxlength' => true])->label('Ссылка на видео') ?>

            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'second')->dropDownList(Yii::$app->params['duration'])->label('Время просмотра') ?>

            <?= $form->field($model, 'budget')->textInput(['type' => 'number']) ?>

            <?= $form->field($model, 'price')->textInput(['type' => 'number'])->label('Цена за 1 просмотр (Рекомендованная ставка ~ <span id="place-average">' . $averagePrice . '</span> поинта)') ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>

</div>
