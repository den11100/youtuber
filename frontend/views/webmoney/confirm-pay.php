<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var int $orderId */
/* @var int $amount */

?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border"><strong>Оплата при помощи WebMoney</strong></div>
            <div class="box-body">
                <div class="alert alert-info alert-dismissible">
                    <h4><i class="icon fa fa-info"></i></h4>
                    <p>Обработка платежей происходит в автоматическом режиме, баллы будут зачислены сразу после оплаты.</p>
                    <p>Совершая оплату, вы подтверждаете что ознакомились и согласны с <a target="_blank" href="<?= Url::to(['/site/offer'])?>">Правилами сервиса</a>.</p>
                </div>

                <p><strong>Пользователь ID:</strong> <?= Yii::$app->user->getId() ?></p>
                <p><strong>Сумма:</strong> <?= $amount ?> р.</p>
                <p><strong>Номер заказа: <?= $orderId ?></strong></p>
            </div>
            <div class="box-footer">
                <form method="POST" action="<?= Yii::$app->params['wm-url-api'] ?>">
                    <input type="hidden" name="LMI_PAYMENT_AMOUNT" value="<?= $amount ?>">
                    <input type="hidden" name="LMI_PAYMENT_DESC_BASE64" value="<?= base64_encode("TubePro оплата счета $orderId")?>">
                    <input type="hidden" name="LMI_PAYMENT_NO" value="<?= $orderId ?>">
                    <input type="hidden" name="LMI_PAYEE_PURSE" value="<?= Yii::$app->params['wm-wallet'] ?>">
                    <!--            <input type="hidden" name="LMI_SIM_MODE" value="0">           -->
                    <input type="submit" value="Оплатить" class="btn btn-success"> или <a href="<?= Url::to(['/order/delete/' . $orderId]) ?>">отменить</a>
                </form>
            </div>
        </div>

    </div>
</div>


