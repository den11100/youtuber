<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

?>

<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-header with-border"><strong>Оплата при помощи WebMoney</strong></div>
            <div class="box-body">
                <div class="alert alert-danger alert-dismissible">
                    <h4><i class="icon fa fa-info"></i></h4>
                    <p>Что то пошло не так.</p>
                    <p><a href="<?= Url::to(['order/create'])?>">Вернутся на страницу оплаты.</a>.</p>
                </div>
            </div>
            <div class="box-footer"></div>
        </div>

    </div>
</div>


