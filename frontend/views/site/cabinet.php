<?php

/* @var $this yii\web\View */
/* @var $contactForm frontend\models\ContactForm */
/* @var int $completeTask */
/* @var int $inProcessTask  */
/* @var int $referals */
/* @var int $currentUserId */
/* @var array $cabinetWatchMovie */

use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Кабинет';
?>

<!-- Info boxes -->
<div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-thumbs-o-up"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Завершенные задания</span>
                <span class="info-box-number"><?= $completeTask ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Задания в работе</span>
                <span class="info-box-number"><?= $inProcessTask ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->


    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Рефералы</span>
                <span class="info-box-number"><?= $referals ?></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

</div>
<!-- /.row -->


<div class="row">
    <div class="col-md-12">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>Видео</h3>
                <p>Добавить задание на просмотр</p>
            </div>
            <div class="icon">
                <i class="fa fa-youtube-play"></i>
            </div>
            <a href="<?= Url::to(['/task/create']) ?>" class="small-box-footer">
                Перейти <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>


<div class="row">

    <div class="col-md-6">
        <div class="box box-success box-solid collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Как заработать поинты?</h3>


                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <p>1) Запустите клиент для автоматического просмотра видео.</p>
                <p>2) Разрешите всплывающие окошки для нашего сайта - что-бы видео открывались.</p>



                <p><a class="run-client" target="_blank" data-url="<?= Url::to(['site/client']) ?>">
                    <i class="fa fa-caret-square-o-right"></i> Запустить клиент
                    <span class="label label-success">v1.1</span>
                    </a>
                </p>
                <div class="callout callout-danger">
                    <p>После того как разрешили всплывающие окна, закройте клиент и запустите заново.</p>
                </div>
                <p><img class="img-responsive" src="/images/vsplivaushie_okna.gif" alt=""></p>


            </div>
            <!-- /.box-body -->
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-warning box-solid collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title">Обращение в тех поддержку</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: none;">
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($contactForm, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($contactForm, 'email') ?>

                <?= $form->field($contactForm, 'body')->textarea(['rows' => 4]) ?>

                <?= $form->field($contactForm, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <!-- /.box-body -->
        </div>

    </div>

</div>



<?php $this->registerJsFile("@web/js/cabinet.js", [
    'depends' => JqueryAsset::className(),
]);
