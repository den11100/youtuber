<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<header class="masthead height-100vh text-center text-white d-flex">
    <div class="container my-auto">
        <div class="row">
            <div class="col-md-6 mx-auto">

                <h1><?= Html::encode($this->title) ?></h1>

                <div class="alert alert-danger">
                    <?= nl2br(Html::encode($message)) ?>
                </div>

            </div>
        </div>
    </div>
</header>
