<?php

/* @var $this yii\web\View */
/* @var $task frontend\models\Task */

use frontend\assets\AppAsset;
use yii\web\JqueryAsset;

AppAsset::register($this);

$this->title = 'Client 1.0.1';
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>TubePro</h1>
            <p>Клиент для просмотра видео в автоматическом режиме. v1.1</p>
            <p>За каждый просмотр вы получаете 1 поинт</p>
            <p>&nbsp;</p>
            <div class="wrap-btns">
            <?php if ($task == 0): ?>
               <p>Сейчас заданий нет, попробуйте закрыть браузер, и запустить клиент заново через некоторое время.</p>
            <?php else: ?>
                <p>
                    <a class="btn btn-success btn-start-client" href="#" data-tasktime="<?= $task['second'] ?>" data-taskid="<?= $task['id'] ?>" data-url="<?= $task['url'] ?>">Запустить</a>
                    <a class="btn btn-success btn-stop-client" href="#">Остановить</a>
                </p>
                <p class="timer-place"></p>
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJsFile('@web/js/client.js', [
        'depends' => JqueryAsset::className(),
]);


