<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Купить, получить просмотры youtube';
?>

<header class="masthead text-center text-white d-flex">
    <div class="container my-auto">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <h1 class="text-uppercase">
                    <strong>Добавим просмотров на ваш канал</strong>
                </h1>
                <hr>
            </div>
            <div class="col-lg-8 mx-auto">
                <p class="text-faded mb-5">Система обмена просмотрами</p>
                <?php if(Yii::$app->user->isGuest): ?>
                    <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Подробнее</a>
                <?php else: ?>
                    <a class="btn btn-primary btn-xl" href="<?= Url::to(['/site/cabinet'])?>">Перейти в кабинет</a>
                <?php endif ?>
            </div>
        </div>
    </div>
    <?php if( Yii::$app->session->hasFlash('success') ): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php endif;?>
    <?php if( Yii::$app->session->hasFlash('error') ): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('error'); ?>
        </div>
    <?php endif;?>
</header>

<section class="bg-primary" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <h2 class="section-heading text-white">Попробуйте совершенно бесплатно</h2>
                <hr class="light my-4">
                <p class="text-faded mb-4">Добавьте видео и получайте просмотры реальных людей</p>
                <?php if (Yii::$app->user->isGuest): ?>
                    <a class="btn btn-light btn-xl" href="<?= Url::to(['/site/signup']) ?>">Попробовать</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Как всё устроено</h2>
                <hr class="my-4">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-diamond text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Добавьте ссылку на ваше видео</h3>
<!--                    <p class="text-muted mb-0">Просто скопируйте ссылку</p>-->
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-dollar text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Установите бюджет</h3>

                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-eye text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Получайте просмотры</h3>

                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-heart text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3"><?= Yii::$app->params['defaultPointsSignup'] ?> поинтов в подарок</h3>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-dark text-white">
    <div class="container text-center">
        <h2 class="mb-4">Получить бесплатные просмотры</h2>
        <?php if (Yii::$app->user->isGuest): ?>
            <a class="btn btn-light btn-xl sr-button" href="<?= Url::to(['/site/signup']) ?>">Регистрация</a>
        <?php endif; ?>
    </div>
</section>

<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <h2 class="section-heading">Остались вопросы</h2>
                <hr class="my-4">
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-4 text-center">
                <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
                <p>&#104;&#101;&#108;&#108;&#111;&#64;&#116;&#117;&#98;&#101;&#112;&#114;&#111;&#46;&#114;&#117;</p>
            </div>
        </div>
    </div>
</section>
