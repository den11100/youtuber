<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Регистрация';
?>

<header class="masthead height-100vh text-center text-white d-flex">
    <div class="container my-auto">
        <div class="row">
            <div class="col-lg-8 mx-auto">

                <h1 class="text-uppercase">Регистрация</h1>


                <?php if (Yii::$app->params['registration-work'] == 1): ?>

                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>

                    <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false) ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-md-4">{image}</div><div class="col-md-8">{input}</div></div>',
                    ]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Регистрация', ['class' => 'btn btn-warning btn-xl', 'name' => 'signup-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                <?php else: ?>

                    <p>Профилакти. попробуйте зайти позже</p>

                <?php endif ?>

            </div>
        </div>
    </div>
</header>
