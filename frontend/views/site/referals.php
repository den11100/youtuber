<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $users common\models\User[] */
/* @var $currentUser common\models\User */

$this->title = 'Рефералы';
?>
<div class="referals">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if($users == []): ?>

        <p>У вас пока нет рефералов. Ваша партнёрская ссылка <input type="text" value="<?= Yii::$app->params['urlSite'] . '/?id_u=' . Yii::$app->user->getId() ?>"></p>

    <?php else: ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-body">
                    <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">

                    </h4>
                    <div class="media">
                        <div class="media-body">

                        <?php foreach ($users as $user): ?>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="info-box bg-aqua">
                                        <span class="info-box-icon"><i class="ion ion-person-add"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text"><?= $user->getPartEmail() ?></span>
                                            <span class="info-box-number"><?= $user->getPointsFromReferal() ?> поинтов</span>

                                            <div class="progress">
                                                <div class="progress-bar" style="width: 100%"></div>
                                            </div>
                                            <span class="progress-description">Принёс всего поинтов за всё время</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                            </div>

                        <?php endforeach; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php endif ?>

    <?php if($currentUser->export_money == 1): ?>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-body">
                        <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                            Премиум привелегии
                        </h4>
                        <div class="media">
                            <div class="media-body">

                                <?php foreach ($users as $user): ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="info-box bg-green">
                                            <span class="info-box-icon"><i class="ion ion-person-add"></i></span>

                                            <div class="info-box-content">
                                                <span class="info-box-text"><?= $user->email ?></span>
                                                <span class="info-box-number"><?= $user->getMoneyFromReferal() ?> р.</span>
                                                <span class="info-box-text">Принёс всего денежных срадств за текущий месяц</span>

                                                <div class="progress">
                                                    <div class="progress-bar" style="width: 100%"></div>
                                                </div>
                                                <?php foreach ($user->getOrdersForCurrentMonth() as $order): ?>
                                                    <span class="progress-description">
                                                        <?php if($order->date_paid): ?>
                                                        <?= Yii::$app->formatter->asDatetime($order->date_paid) ?> - <?= $order->amount ?> р.
                                                        <?php endif; ?>
                                                    </span>
                                                <?php endforeach; ?>

                                            </div>
                                            <!-- /.info-box-content -->
                                        </div>
                                        <!-- /.info-box -->
                                    </div>
                                </div>
                                <?php endforeach; ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php endif ?>



</div>
