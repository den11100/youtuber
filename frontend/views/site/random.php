<?php

/* @var $this yii\web\View */
/* @var $task frontend\models\Task */

use frontend\assets\AppAsset;
use yii\web\JqueryAsset;



$this->title = 'Client 1.0.1';
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>TubePro</h1>

            <p>&nbsp;</p>

            <?php if (isset($task)): ?>

                <?php if ($task == 0): ?>
                    <p>Сейчас заданий нет, попробуйте закрыть браузер, и запустить клиент заного через некоторое время.</p>
                <?php else: ?>
                    <p>
                        <a class="btn btn-success btn-start-client" href="#" data-tasktime="<?= $task['second'] ?>" data-taskid="<?= $task['id'] ?>" data-url="<?= $task['url'] ?>">Запустить</a>
                        <a class="btn btn-success btn-stop-client" href="#">Остановить</a>
                    </p>
                    <p class="timer-place"></p>
                <?php endif; ?>

            <?php else: ?>

                <form method="post">
                    <p><input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" /></p>
                    <p><input name="access-key" class="form-control" type="text"></p>
                    <p><button name="btn-access-key" type="submit" class="btn btn-warning">Отправить</button></p>
                </form>

            <?php endif; ?>

        </div>
    </div>
</div>

<?php if (isset($task)) {
    AppAsset::register($this);
    $this->registerJsFile('@web/js/random-page.js', [
        'depends' => JqueryAsset::className(),
    ]);
}
