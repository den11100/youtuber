<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Войти в кабинет';

?>

<header class="masthead height-100vh text-center text-white d-flex">
    <div class="container my-auto">
        <div class="row">
            <div class="col-lg-8 mx-auto">

                <h1 class="text-uppercase"><?= Html::encode($this->title) ?></h1>

                <?php if (Yii::$app->params['registration-work'] == 1): ?>

                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                        <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'placeholder' => 'Email'])->label(false) ?>

                        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false) ?>

                        <?= $form->field($model, 'rememberMe')->checkbox() ?>

                        <div style="margin:1em 0">
                            Восстановить пароль <?= Html::a('перейти', ['site/request-password-reset']) ?>.
                        </div>

                        <div class="form-group">
                            <?= Html::submitButton('Вход', ['class' => 'btn btn-warning btn-xl', 'name' => 'login-button']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>

                <?php else: ?>

                    <p>Профилакти. попробуйте зайти позже</p>

                <?php endif ?>

            </div>
        </div>
    </div>
</header>

