<?php

/* @var $this yii\web\View */
/* @var $orderId integer */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Helper;

?>

<div class="box box-warning">

    <div class="box-header with-border"><strong>С помощью WebMoney</strong></div>

    <?= Html::beginForm(Url::to(['/webmoney/create']), 'post', ['id' => 'webmoney-method']) ?>

        <div class="box-body">
            <div class="form-group required">
                <label class="control-label" for="amount">Сумма</label>
                <?= Html::dropDownList('amount', [1], Yii::$app->params['amount-pay'], ['id' => 'amount', 'class'=>'form-control']) ?>
            </div>
        </div>
        <div class="box-footer">
            <?= Html::submitButton('Оплатить', ['class' => 'btn btn-success', 'name' => 'submit-wm']) ?>
        </div>

    <?= Html::endForm() ?>

</div>