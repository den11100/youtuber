<?php

/* @var $this yii\web\View */
/* @var $orders \frontend\models\Order[] */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Helper;

$this->title = 'История платежей';
?>

<h1>История платежей</h1>

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-body">
                <p><a href="<?= Url::to(['/order/create'])?>" class="btn btn-success">Пополнить баланс</a></p>
                <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;"></h4>
                <div class="media">
                    <div class="media-body">

                        <?php foreach ($orders as $order): ?>

                            <div class="callout callout-warning">
                                <p><i class="fa fa-rub"></i> id: <?= $order->id ?>  Дата: <?= Yii::$app->formatter->asDatetime($order->date_paid) ?> Сумма:<?= $order->amount ?>р. <?= $order->getNameMethod(); ?></p>
                            </div>

                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
