<?php

/* @var $this yii\web\View */
/* @var $currentUser \common\models\User */
/* @var $amount integer */
/* @var $method integer */
/* @var $orderId integer */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Helper;

$this->title = 'Пополнить баланс';
?>

<div class="row">
    <div class="col-md-12">
        <h1><?= $this->title ?></h1>
        <div class="alert alert-info alert-dismissible">
            <h4><i class="icon fa fa-info"></i></h4>
            <p>Обработка платежей происходит в автоматическом режиме, баллы будут зачислены сразу после оплаты.</p>
            <p>Совершая оплату, вы подтверждаете что ознакомились и согласны с <a target="_blank" href="<?= Url::to(['/site/offer'])?>">Правилами сервиса</a>.</p>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">

        <?php if ($orderId == 0): ?>

            <div class="box box-primary">
                <div class="box-header with-border"><strong>Через Yandex</strong></div>
                <?= Html::beginForm(Url::to(['/order/create']), 'post', ['id' => 'select-method']) ?>
                <div class="box-body">
                    <div class="form-group required">
                        <?php
                        $options = [
                            'id' => 'method',
                            'item'=>function ($index, $label, $name, $checked, $value) {
                                $check = $checked ? ' checked="checked"' : '';
                                return '<div class="radio"><label><input type="radio" name="'.$name.'" value="'.$value.'" '.$check.'/>'.$label.'</label></div>';
                            }
                        ];
                        ?>
                        <?= Html::radioList('method', [1], Yii::$app->params['method-pay'], $options) ?>
                    </div>

                    <div class="form-group required">
                        <label class="control-label" for="amount">Сумма</label>
                        <?= Html::dropDownList('amount', [1], Yii::$app->params['amount-pay'], ['id' => 'amount', 'class'=>'form-control']) ?>
                    </div>
                </div>
                <div class="box-footer">
                    <?= Html::submitButton('Оплатить', ['class' => 'btn btn-success', 'name' => 'submit-prepay']) ?>
                </div>
                <?= Html::endForm() ?>
            </div>

        <?php else: ?>

            <div class="box box-primary">
                <div class="box-header with-border"></div>
                <div class="box-body">
                    <p><strong>Пользователь ID:</strong> <?= $currentUser->id ?></p>
                    <p><strong>Способ оплаты:</strong> <?= Helper::getLabelmethodPay($method) ?></p>
                    <p><strong>Сумма:</strong> <?= $amount ?> р.</p>
                    <p><strong>Номер заказа: <?= $orderId ?></strong></p>
                </div>
                <div class="box-footer">
                    <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
                        <input type="hidden" name="receiver" value="<?= Yii::$app->params['number-yandex-money'] ?>">
                        <input type="hidden" name="formcomment" value="TubePRO: Пополнение баланса">
                        <input type="hidden" name="short-dest" value="TubePRO: Пополнение баланса">
                        <input type="hidden" name="label" value="<?= $orderId ?>">
                        <input type="hidden" name="quickpay-form" value="shop">
                        <input type="hidden" name="targets" value="Пополнение баланса пользователя #<?= $currentUser->id ?> на сумму <?= $amount ?> р.">
                        <input type="hidden" name="paymentType" value="<?= Helper::getValueMethodForYandexMoney($method) ?>">
                        <input type="hidden" name="sum" value="<?= $amount ?>.00">
                        <input type="submit" class="btn btn-success" value="Оплатить"> или <a href="<?= Url::to(['/order/delete/'.$orderId]) ?>">отменить</a>
                    </form>
                </div>
            </div>

        <?php endif; ?>

    </div>
    <div class="col-md-6">
        <?= $this->render('_webmoney', [
            'orderId' => $orderId,
        ]) ?>
    </div>
</div>
