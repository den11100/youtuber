<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\widgets\leftMenu\LeftMenu;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use frontend\widgets\pointView\PointView;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <?= Html::csrfMetaTags() ?>

    <!-- Favicons -->
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicons/favicon-16x16.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300i,400i,600i&amp;subset=cyrillic">


    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="layout-boxed sidebar-mini skin-purple-light" style="height: auto; min-height: 100%;">
<?php $this->beginBody() ?>


<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>T</b>PRO</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Tube</b>PRO</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <?php if(!Yii::$app->user->isGuest): ?>
                    <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <a class="run-client" target="_blank" data-url="<?= Url::to(['site/client']) ?>">
                                <i class="fa fa-caret-square-o-right"></i> Запустить клиент
                                <span class="label label-success">v1.1</span>
                            </a>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                                <?= Html::beginForm(['/site/logout'], 'post') ?>
                                <?= Html::submitButton('Выйти: ' . Yii::$app->user->identity->email, ['class' => 'btn btn-flat logout'])?>
                                <?= Html::endForm() ?>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>

        </nav>
    </header>
    <?php if(!Yii::$app->user->isGuest): ?>
<!--     Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel text-center">
                 <?= PointView::widget([]) ?>
            </div>

            <?= LeftMenu::widget(['items' => [

                ['label' => 'Панель управления', 'icon' => ' fa-dashboard', 'url' => ['/site/cabinet']],

                ['label' => 'ЗАДАНИЯ', 'options' => ['class' => 'header']], // here
                ['label' => 'Просмотр всех заданий', 'icon' => ' fa-bar-chart', 'url' => ['/task/index']],
                ['label' => 'Добавить задание', 'icon' => ' fa-youtube-play', 'url' => ['/task/create']],

                ['label' => 'ПОПОЛНИТЬ БАЛАНС', 'options' => ['class' => 'header']], // here
                ['label' => 'Оплата', 'icon' => ' fa-dollar', 'url' => ['/order/create']],
                ['label' => 'История платежей', 'icon' => ' fa-search-plus', 'url' => ['/order/index']],

                ['label' => 'РЕФЕРАЛЬНАЯ СИСТЕМА', 'options' => ['class' => 'header']], // here
                ['label' => 'Рефералы', 'icon' => ' fa-star-o', 'url' => ['/site/referals']],
                ['label' => 'Правила сервиса', 'icon' => ' fa-book', 'url' => ['/site/offer']],
//                    'items' => [
//                        [
//                            'label' => 'Mailbox',
//                            'icon' => 'envelope-o',
//                            'url' => ['/mailbox'],
//                            'template'=>'<a href="{url}">{icon} {label}<span class="pull-right-container"><small class="label pull-right bg-yellow">123</small></span></a>'
//                        ],
//                    ]
                ['label' => 'ВАША РЕФ. ССЫЛКА', 'options' => ['class' => 'header']], // here

            ]]);

            ?>
            <input type="text" value="http://tubepro.ru/?id_u=1" style="margin-left: 15px;">
        </section>
    </aside>
    <?php endif; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?= Alert::widget() ?>
            <?= $content ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; <?= date('Y') ?></strong> All rights
        reserved.
    </footer>



</div>
<!-- ./wrapper -->











<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
