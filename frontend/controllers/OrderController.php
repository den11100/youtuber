<?php

namespace frontend\controllers;

use common\models\Helper;
use common\models\User;
use frontend\controllers\behaviors\CheckSiteOffBehavior;
use frontend\models\Order;
use Yii;
use yii\web\NotFoundHttpException;

class OrderController extends \yii\web\Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            CheckSiteOffBehavior::className(),
        ];
    }


    public function actionIndex()
    {
        $currentUserId = Yii::$app->user->getId();

        $orders = Order::find()->where(['status' => Order::ORDER_STATUS_PAID, 'user_id' => $currentUserId])->orderBy(['id' => SORT_ASC])->all();

        return $this->render('index', [
            'orders' => $orders,
        ]);
    }

    public function actionCreate()
    {
        $currentUser = Yii::$app->user->identity;
        $amount = '';
        $method = '';
        $orderId = 0;

        if (isset($_POST['submit-prepay'])) {
            $amount = intval(Yii::$app->request->post('amount'));
            $method = intval(Yii::$app->request->post('method'));

            $order = new Order();
            $order->user_id = $currentUser->id;
            $order->amount = $amount;
            $order->method = $method;
            $order->status = Order::ORDER_STATUS_NEW;
            if ($order->save()) {
                $orderId = $order->id;
            }
        }

        return $this->render('create',[
            'currentUser' =>  $currentUser,
            'amount' =>  $amount,
            'method' =>  $method,
            'orderId' =>  $orderId,
        ]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the '/order/create' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        Helper::checkWhoIsAuthor(Order::className(), $id);

        $model = $this->findModel($id);

        if ($model->status == Order::ORDER_STATUS_PAID) {
            Yii::$app->session->setFlash('danger', 'Нельзя удалить оплаченный заказ');
            return $this->redirect(['/order/create']);
        } else {
            $model->delete();
            return $this->redirect(['/order/create']);
        }
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function beforeAction($action) {
        if($action->id == 'yandex-paid-info') {
            Yii::$app->request->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Приём оповещений от яндекса
     * @return string
     */
    public function actionYandexPaidInfo()
    {
        if (isset($_POST["notification_type"])) {
            Yii::info("----clean-response " . json_encode($_POST), 'pay');
            $operation_id = $_POST['operation_id']; //- номер операция
            $amount = $_POST['amount']; //- количество денег, которые поступят на счет получателя
            $currency = $_POST['currency'];
            $datetime = $_POST['datetime'];
            $sender = $_POST['sender']; //-оплата производится через Яндекс Деньги, то этот параметр сод. номер кошелька покупателя
            $label = $_POST['label'];
            $codepro = $_POST['codepro'];
            if ($codepro == "true") {
                Yii::info("----codepro", 'pay');
                die('codepro');
            }
            if(isset($_POST['unaccepted'])) {
                $unaccepted = $_POST['unaccepted'];
                if ($unaccepted == "true") {
                    Yii::info("----unaccepted", 'pay');
                    die('unaccepted');
                }
            }
            if(isset($_POST['withdraw_amount'])) {
                $withdraw_amount = $_POST['withdraw_amount'];
            }
            $notification_type = $_POST['notification_type'];
            $post_sha1_hash = $_POST['sha1_hash'];

            $notification_secret = Yii::$app->params['yandex-secret-word']; // секретное слово
            //Yii::info("line2", 'pay');

            $sha1 = sha1( $notification_type .
                '&'. $operation_id .
                '&'. $amount .
                '&'. $currency .
                '&'. $datetime .
                '&'. $sender .
                '&'. $codepro .
                '&'. $notification_secret.
                '&'. $label);

            if ($sha1 != $post_sha1_hash ) {
                Yii::info("----sha1 no", 'pay');
                exit('no verification sha1');
            } else {
                // Yii::info("line3", 'pay');
                // тут код на случай, если проверка прошла успешно
                $orderId = $label;
                $order = Order::find()->where(['id' => intval($orderId)])->one();
                if ($order) {
                    if ($order->status == Order::ORDER_STATUS_NEW) {
                        $order->status = Order::ORDER_STATUS_PAID;
                        $order->amount = round($amount, 0);
                        $order->operation_number = $operation_id;
                        if (isset($withdraw_amount)) {
                            $order->info = $withdraw_amount;
                        }
                        if($sender) {
                            $order->yandex_wallet = $sender;
                        }

                        Yii::info("--amount--".$order->amount, 'pay');
                        if ($order->save()) {
                            /* @var $user \common\models\User */
                            $user = User::find()->where(['id' => $order->user_id])->one();
                            $user->addPointsAfterPaid(round($amount, 0));
                            $user->save();
                        }
                    }
                }
            }


        } // end if response

        return $this->render('yandex-paid-info.php');
    }

    // Тест начисления поинтов от реферала
//    public function actionTest($id)
//    {
//        $order = $this->findModel($id);
//        $user = User::find()->where(['id' => $order->user_id])->one();
//        $user->addPointsAfterPaid(100);
//        $user->save();
//    }

}
