<?php
namespace frontend\controllers;

use common\models\User;
use frontend\controllers\behaviors\CheckSiteOffBehavior;
use frontend\models\Task;
use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            CheckSiteOffBehavior::className(),
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'client', 'cabinet', 'login', 'offer', 'referals'],
                'rules' => [
                    [
                        'actions' => ['signup', 'login', 'random'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'client', 'cabinet', 'offer', 'referals'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'home',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($id_u = 0)
    {
        $id_u = intval($id_u);
        if ($id_u) {
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'parent_user',
                'value' => $id_u,
            ]));
        }

        $this->layout = '@frontend/views/layouts/home.php';

        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = '@frontend/views/layouts/home.php';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/site/cabinet']);
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionCabinet()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $user = Yii::$app->user->identity;

        $referals = User::find()->where(['parent_user' => $user->id])->count();

        $completeTask = Task::find()->where(['status' => Task::STATUS_FINISH, 'user_id' => $user->id])->count();
        $inProcessTask = Task::find()->where(['status' => Task::STATUS_WORK, 'user_id' => $user->id])->count();

        $contactForm = new ContactForm();
        if ($contactForm->load(Yii::$app->request->post()) && $contactForm->validate()) {
            if ($contactForm->sendEmail(Yii::$app->params['adminEmail'], $user->id)) {
                Yii::$app->session->setFlash('success', 'Сообщение отправлено.');
            } else {
                Yii::$app->session->setFlash('error', 'Сообщение не отправлено.');
            }
            return $this->refresh();
        }

        return $this->render('cabinet', [
            'completeTask' => $completeTask,
            'inProcessTask' => $inProcessTask,
            'referals' => $referals,
            'contactForm' => $contactForm,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionReferals()
    {
        $currentUser = Yii::$app->user->identity;

        $users = User::find()->where(['parent_user' => Yii::$app->user->identity->getId()])->all();

        return $this->render('referals',[
            'users' => $users,
            'currentUser' => $currentUser,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionOffer()
    {
        return $this->render('offer');
    }

    /**
     * Displays client page.
     *
     * @return mixed
     */
    public function actionClient()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(['/']);
        }

        $taskListArray = Task::find()->where(['status' => Task::STATUS_WORK])->asArray()->all();

        if ($taskListArray == []) {
            $task = 0;
        } else {
            $idsActiveTask = ArrayHelper::getColumn($taskListArray, 'id');
            $randomId = $idsActiveTask[array_rand($idsActiveTask, 1)];
            $task = Task::find()->where(['id' => $randomId])->asArray()->one();
        }

        return $this->renderAjax('client', [
            'task' => $task,
        ]);
    }

    public function actionRandom()
    {
        if(isset($_POST['btn-access-key'])) {

            if (isset($_POST['access-key']) && Yii::$app->request->post('access-key') == Yii::$app->params['access-key']) {

                $taskListArray = Task::find()->where(['status' => Task::STATUS_WORK])->asArray()->all();

                if ($taskListArray == []) {
                    $task = 0;
                } else {
                    $idsActiveTask = ArrayHelper::getColumn($taskListArray, 'id');
                    $randomId = $idsActiveTask[array_rand($idsActiveTask, 1)];
                    $task = Task::find()->where(['id' => $randomId])->asArray()->one();
                }

                return $this->renderAjax('random', [
                    'task' => $task,
                ]);
            } else {
                return $this->renderAjax('random');
            }

        } else {
            return $this->renderAjax('random');
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $this->layout = '@frontend/views/layouts/home.php';

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(['/site/cabinet']);
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = '@frontend/views/layouts/home.php';

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверьте свою почту и папку спам. Следуйте инструкции в письме');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Мы не смоги отправить письмо.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = '@frontend/views/layouts/home.php';

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль установлен.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
