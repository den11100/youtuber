<?php

namespace frontend\controllers;

use common\models\Helper;
use common\models\User;
use frontend\controllers\behaviors\AccessBehavior;
use frontend\controllers\behaviors\CheckSiteOffBehavior;
use Yii;
use frontend\models\Task;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\httpclient\Client;
use yii\db\Query;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            AccessBehavior::className(),
            CheckSiteOffBehavior::className(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($action->id == 'success-attempt' || $action->id == 'next-video' || $action->id == 'success-attempt-random-watch' || $action->id == 'next-video-random-watch') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Task::find()->where(['user_id' => Yii::$app->user->getId()]),
            'pagination' => [
                'pageSize' => 200,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionView($id)
//    {
//        Helper::checkWhoIsAuthor(Task::className(), $id);
//
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->isGuest) {
            /* @var $user \common\models\User */
            $user = Yii::$app->user->identity;
        }

        $query = new Query;
        $query->from('{{%task}}');
        $query->where(['status' => Task::STATUS_WORK, 'second' => 45]);
        $averagePrice = round($query->average('price'));

        if ($averagePrice == 0) {
            $averagePrice = 1;
        }

        $model = new Task();

        if ($model->load(Yii::$app->request->post()) ) {

            $model->user_id = Yii::$app->user->getId();
            $model->status = Task::STATUS_WORK;
            $model->type = Task::TYPE_WATCH;

            $model->quantity = 0;
            $model->order_quantity = intdiv($model->budget, $model->price);
            $model->rang = (($model->second / ($model->second/10)) * $model->price) - $model->getRateTask();

            $result = $user->chekPointsAvailable($model->budget);
            if ($result == false) {
                Yii::$app->session->setFlash('warning', 'Не хватает поинтов, пополните баланс');
                return $this->redirect(['/order/create']);
            }

            if($model->save()) {
                $user->deductPoints($model->budget);
                $user->save();
            }
            return $this->redirect(['task/index']);
        }

        return $this->render('create', [
            'model' => $model,
            'averagePrice' => $averagePrice,
        ]);
    }

    public function actionAjaxAveragePrice()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post('seconds')) {

            $query = new Query;
            $query->from('{{%task}}');
            $query->where(['status' => Task::STATUS_WORK, 'second' => Yii::$app->request->post('seconds')]);
            $averagePrice = round($query->average('price'));

            if ($averagePrice == 0) {
                $averagePrice = 1;
            }

            return $averagePrice;
        }
        return true;
    }

    public function actionAjaxGetNameVideo()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post('urlVideo')) {
            $urlGet = "http://www.youtube.com/oembed?url=" . Yii::$app->request->post('urlVideo') . "&format=json";
        }

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($urlGet)
            ->send();
        return $response->content;
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionUpdate($id)
//    {
//        if (!Yii::$app->user->isGuest) {
//            /* @var $user \common\models\User */
//            $user = Yii::$app->user->identity;
//        }
//
//        Helper::checkWhoIsAuthor(Task::className(), $id);
//
//        $model = $this->findModel($id);
//        $model->budget = intval($model->budget);
//        $model->price = intval($model->price);
//
//        if ($model->load(Yii::$app->request->post())) {
//
//            $model->budget = intval($model->budget);
//            $model->price = intval($model->price);
//
//            if ($model->budget < $model->price) {
//                Yii::$app->session->setFlash('warning', 'Увеличте бюджет');
//                return $this->refresh();
//            }
//
//            $model->order_quantity = intdiv($model->budget, $model->price);
//            $model->rang = (($model->second / ($model->second/10)) * $model->price) - $model->getRateTask();
//
//            if ($model->quantity < $model->order_quantity ) {
//                $model->status = Task::STATUS_WORK;
//            }
//
//            $oldBudget = $model->getOldAttribute('budget');
//            $newValueBudgetInUpdate = $model->budget - $oldBudget;
//            $result = $user->chekPointsAvailable($newValueBudgetInUpdate);
//            if ($result == false) {
//                Yii::$app->session->setFlash('warning', 'Не хватает поинтов, пополните баланс');
//                return $this->redirect(['/order/create']);
//            }
//
//
//            if ($model->save()) {
//                $user->deductPoints($newValueBudgetInUpdate);
//                $user->save();
//            }
//
//            return $this->redirect(['index']);
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        Helper::checkWhoIsAuthor(Task::className(), $id);

        $model = $this->findModel($id);

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Subtraction quantity task watch
     * @return string
     */
    public function actionSuccessAttempt()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post('id')) {

            $id = intval(Yii::$app->request->post('id'));

            $task = Task::find()->where(['id' => $id])->andWhere(['status' => Task::STATUS_WORK])->one();

            Yii::$app->response->format = Response::FORMAT_JSON;

            if($task) {

                $task->quantity = $task->quantity + 1;

                if ($task->quantity == $task->order_quantity) {
                    $task->status = Task::STATUS_FINISH;
                }

                $task->save();

                if (!Yii::$app->user->isGuest) {
                    $user = User::find()->where(['id' => Yii::$app->user->identity->getId()])->one();
                    $user->point = $user->point + 1;
                }
                $user->save();
                return "done";
            } else {
                return "status task bad or no id";
            }
        }
    }

    /**
     * Успешный просмотр для рандомного показа видео без авторизации
     * @return string
     */
    public function actionSuccessAttemptRandomWatch()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post('id')) {

            $id = intval(Yii::$app->request->post('id'));

            $task = Task::find()->where(['id' => $id])->andWhere(['status' => Task::STATUS_WORK])->one();

            Yii::$app->response->format = Response::FORMAT_JSON;

            if($task) {

                $task->quantity = $task->quantity + 1;

                if ($task->quantity == $task->order_quantity) {
                    $task->status = Task::STATUS_FINISH;
                }

                $task->save();

                return "done";
            } else {
                return "status task bad or no id";
            }
        }
    }

    public function actionNextVideo()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post('pin')) {

            $prevVideoId = intval(Yii::$app->request->post('prevVideoId'));

            $session = Yii::$app->session;
            $session['ids'] = new \ArrayObject;
            $session["ids.$prevVideoId"] = $prevVideoId;

            foreach ($session as $name => $value) {
                if(substr($name,0,4) === "ids."){
                    $arrayIds[] = $value;
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            $task = Task::find()->where(['status' => Task::STATUS_WORK])->andWhere(['not in', 'id', $arrayIds])->orderBy('rang DESC')->asArray()->one();

            if ($task) {
                return $task;
            } else {
                return ["url" => "ended"];
            }
        }
    }

    /**
     * Запрос следующего видео для рандомного просмотра без авторизации
     * @return array|null|\yii\db\ActiveRecord
     */
    public function actionNextVideoRandomWatch()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post('pin')) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            $task = Task::getRandomFilm(Task::WITH_CURRENT_USER);

            if ($task) {
                return $task;
            } else {
                return ["url" => 'ended'];
            }
        }
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
