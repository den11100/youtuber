<?php

namespace frontend\controllers\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\Controller;
/**
 * @author dn
 */
class CheckSiteOffBehavior extends Behavior
{
    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'checkSiteOff',
        ];
    }

    public function checkSiteOff()
    {
        if (Yii::$app->controller->action->id == "yandex-paid-info") {
            return true;
        }

        if (Yii::$app->controller->action->id == "my-secret-result-url") {
            return true;
        }

        if (Yii::$app->params['registration-work'] === 0) {
            if (!Yii::$app->user->isGuest) {
               return Yii::$app->user->logout();
            }
        }
    }
}
