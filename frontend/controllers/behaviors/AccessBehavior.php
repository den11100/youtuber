<?php

namespace frontend\controllers\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\Controller;
/**
 * @author dn
 */
class AccessBehavior extends Behavior
{
    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'checkAccess',
        ];
    }
    
    public function checkAccess()
    {
        if (Yii::$app->user->isGuest) {

            if (Yii::$app->controller->action->id == "success-attempt-random-watch") {
                return true;
            } elseif (Yii::$app->controller->action->id == 'next-video-random-watch') {
                return true;
            } else {
                return Yii::$app->controller->redirect(['/site/index']);
            }

        }
    }
}
