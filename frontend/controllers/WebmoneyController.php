<?php

namespace frontend\controllers;

use common\models\Helper;
use common\models\User;
use yii\filters\VerbFilter;
use frontend\controllers\behaviors\CheckSiteOffBehavior;
use frontend\models\Order;
use frontend\models\Webmoney;
use Yii;
use yii\web\NotFoundHttpException;

class WebmoneyController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            CheckSiteOffBehavior::className(),
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'my-secret-result-url' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Создаём счёт в нашей системе и редиректим на страницу подтверждения оплаты webmoney
     * @return \yii\web\Response
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            die('Access denied');
        }

        if (isset($_POST['submit-wm'])) {
            $amount = intval(Yii::$app->request->post('amount'));

            $order = new Order();
            $order->user_id = Yii::$app->user->getId();
            $order->amount = $amount;
            $order->method = Order::METHOD_PAY_WEBMONEY;
            $order->status = Order::ORDER_STATUS_NEW;
            if ($order->save()) {
                $orderId = $order->id;
                return $this->redirect(['/webmoney/confirm-pay', 'orderId' => $orderId, 'amount' => $amount]);
            }

            Yii::$app->session->setFlash('danger', "Ошибка сохранения счёта, попробуйте другой способ оплаты.");
            return $this->redirect('/order/create');
        }
    }

    public function actionConfirmPay($orderId, $amount)
    {
        return $this->render('confirm-pay', [
           'orderId' => $orderId,
           'amount' => $amount,
        ]);
    }

    // Проверка
    public function actionMySecretResultUrl()
    {
        //Yii::$app->controller->enableCsrfValidation = false;
        if (isset($_POST['LMI_PAYMENT_AMOUNT'])) {

            //кошелек на который будут перечислены средства
            $wm_purce = Yii::$app->params['wm-wallet'];

            $orderId = Helper::cleanData($_POST['LMI_PAYMENT_NO']);
            $order = Order::findOne(intval($orderId));

            if (!$order) {
                exit('Order not found');
            }

            // предварительный запрос от WebMoney
            if (isset($_POST['LMI_PREREQUEST']) && $_POST['LMI_PREREQUEST'] == 1) {
                Yii::info("WEBMONEY__PRE" . "\n_POST_\n" . json_encode($_POST), 'pay');
                $result = Webmoney::checkDataFromWm($_POST, $order, $wm_purce);
                return $result;
            }

            // Подтверждение оплаты от WebMoney
            $LMI_SECRET_KEY = Yii::$app->params['wm-secret-key'];

            $str_hash = $wm_purce.
                strval($order->amount).".00".
                $order->id.
                $_POST['LMI_MODE'].
                $_POST['LMI_SYS_INVS_NO'].
                $_POST['LMI_SYS_TRANS_NO'].
                $_POST['LMI_SYS_TRANS_DATE'].
                $LMI_SECRET_KEY.
                $_POST['LMI_PAYER_PURSE'].
                $_POST['LMI_PAYER_WM'];

            if (strtoupper(hash('sha256', $str_hash)) === $_POST['LMI_HASH']) {

                $order->status = Order::ORDER_STATUS_PAID;
                $order->operation_number = isset($_POST['LMI_PAYER_WM']) ? $_POST['LMI_PAYER_WM'] : 'no';
                if($order->save()) {
                    $user = User::find()->where(['id' => $order->user_id])->one();
                    $user->addPointsAfterPaid(round($order->amount));
                    $user->save();
                }
                Yii::info("WEBMONEY__CHANGE__STATUS" . "\n_MODEL_\n" . json_encode($order->toArray()) . "\n_POST_\n" . json_encode($_POST), 'pay');

            } else {

                Yii::info("WEBMONEY__HASH__NO" . "\n_MODEL_\n" . json_encode($order->toArray()) . "\n_POST_\n" . json_encode($_POST), 'pay');
                exit();

            }
        }
        return true;
    }

    // Пост запрос от WebMoney в случае успешной оплаты
    public function actionSuccess()
    {
        Yii::$app->session->setFlash('success', "Платёж подтверждён");
        return $this->redirect(['order/index']);
    }

    public function actionError()
    {
        return $this->render('error');
    }

}
