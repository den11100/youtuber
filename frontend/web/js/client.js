'use strict';

$('.btn-start-client').click(function () {

    $('.btn-start-client').hide();
    $('.btn-stop-client').show();

    var taskId = $(this).attr('data-taskid');
    var firstUrl = $(this).attr('data-url');
    var taskSeconds = $(this).attr('data-tasktime');


    function watchtv(taskId, taskUrl, taskSeconds) {

        $('.timer-place').text(taskSeconds);

        var _Seconds = taskSeconds;
        var myint;
        myint = setInterval(function() { // запускаем интервал
            if (_Seconds > 0) {
                _Seconds--; // вычитаем 1
                $('.timer-place').text(_Seconds); // выводим получившееся значение в блок
            } else {
                clearInterval(myint); // очищаем интервал, чтобы он не продолжал работу при _Seconds = 0
               console.log('End!');
            }
        }, 1000);

        var newWin = window.open(taskUrl);

        var timerId = setTimeout(function () {
            newWin.close();
            successfulAttempt(taskId);
            nextVideo(taskId);
        }, taskSeconds * 1000);


        $('.btn-stop-client').click(function () {
            clearTimeout(timerId);
            clearInterval(myint);
            newWin.close();
            $('.btn-start-client').show();
            $('.btn-stop-client').hide();
        });

    }

    watchtv(taskId, firstUrl, taskSeconds);

    function successfulAttempt(id) {
        $.ajax(
            {
                url:'/task/success-attempt',
                data: {id:id},
                type: 'POST',
                success: function (res) {
                },
                error: function () {
                    alert('Error attempt');
                }
            }
        );
    }

    function nextVideo(id) {
        $.ajax(
            {
                url:'/task/next-video',
                data: {
                    pin:"123",
                    prevVideoId:id,
                },
                type: 'POST',
                success: function (res) {
                    if(res.url == "ended") {
                        $(".wrap-btns").html("<p>Сейчас заданий нет, попробуйте закрыть браузер, и запустить клиент заново через некоторое время.</p>");
                        console.log("konec");
                    } else {
                        watchtv(res.id, res.url, res.second);
                    }
                },
                error: function () {
                    alert('Error next-video');
                }
            }
        );
    }


});
