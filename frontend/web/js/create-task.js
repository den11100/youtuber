'use strict';

$(document).ready(function () {

    $('#task-url').change(function () {
        var urlVideo = $(this).val();
        $.ajax(
            {
                url:'/task/ajax-get-name-video',
                data: {
                    urlVideo:urlVideo,
                },
                type: 'POST',
                success: function (res) {
                    if (res == "Not Found") {
                        $('#task-description').val('неправильная ссылка');
                    } else {
                        var obj = JSON.parse(res);
                        $('#task-description').val(obj.title);
                    }

                },
                error: function () {

                }
            }
        );
    });

    $('#task-second').change(function () {
        var secondsValue = $(this).val();
        var placeAverage = $('#place-average');
        $.ajax(
            {
                url:'/task/ajax-average-price',
                data: {
                    seconds:secondsValue
                },
                type: 'POST',
                success: function (res) {
                    placeAverage.text(res);
                },
                error: function () {

                }
            }
        );
    });

});





