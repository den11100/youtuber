<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "login_info".
 *
 * @property int $id
 * @property int $user_id
 * @property string $datetime_login
 * @property string $ip
 *
 * @property User $user
 */
class LoginInfo extends \common\models\LoginInfo
{

}
