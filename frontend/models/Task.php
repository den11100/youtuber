<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $created
 * @property int $user_id
 * @property string $url
 * @property string $description
 * @property int $second
 * @property int $budget
 * @property int $price
 * @property int $quantity
 * @property int $type
 * @property int $status
 *
 * @property User $user
 */
class Task extends \common\models\Task
{

}
