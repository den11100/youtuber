<?php

namespace frontend\models;

use common\models\Helper;
use Yii;

/**
 *
 */
class Webmoney extends \yii\base\Model
{
    /**
     * Сравниваем номер кошелька и сумму пришёдшие от WebMoney с тем что у нас в базе у заказа.
     * @param $order \frontend\models\Order
     * @param string $wm_purce
     * @param array $post
     * @return string
     */
    public static function checkDataFromWm($post, $order, $wm_purce)
    {
        if ($post['LMI_PAYEE_PURSE'] == $wm_purce && intval($post['LMI_PAYMENT_AMOUNT']) == $order->amount) {
            return "YES";
        }

        return "NO - different amount";
    }
}

