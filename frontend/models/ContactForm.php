<?php

namespace frontend\models;

use common\models\Helper;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $body;
    public $verifyCode;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email and body are required
            [['name', 'email', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
            [['body'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'Email',
            'body' => 'Сообщение',
            'verifyCode' => 'Проверочный код',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @param integer $userId
     * @return bool whether the email was sent
     */
    public function sendEmail($email, $userId)
    {
        $message = "<p>User_id:$userId</p><p>Email:$this->email</p>" . Helper::cleanData($this->body);

        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setSubject('Форма связи TubePro')
            ->setHtmlBody($message)
            ->send();
    }
}
