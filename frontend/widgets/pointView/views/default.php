<?php

/* @var $this yii\web\View */
/* @var $currentUser \common\models\User */

use yii\widgets\DetailView;
use yii\helpers\Url;

?>
<?php if(!Yii::$app->user->isGuest): ?>
    <p><?= $currentUser->email ?></p>
    <p>Поинты: <?= $currentUser->point ?> <a href="<?= Url::to(['/order/create']) ?>"><small class="label bg-green">+</small></a></p>
<?php endif; ?>
