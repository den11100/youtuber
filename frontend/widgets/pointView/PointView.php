<?php

namespace frontend\widgets\pointView;

use Yii;
use yii\base\Widget;

class PointView extends Widget
{
    public function init()
    {
        return parent::init();
    }

    public function run()
    {
        $currentUser = Yii::$app->user->identity;

        return $this->render('default',[
            'currentUser' => $currentUser,
        ]);
    }
}
