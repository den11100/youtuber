<?php

namespace backend\controllers;

use backend\models\Vkposts;
use common\models\Helper;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class VkController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'mark-watched-video' => ['post'],
                ],
            ],
        ];
    }

    public $enableCsrfValidation = false;

    public function actionMarkWatchedVideo()
    {
        if (Yii::$app->request->isPost) {

            $token = Yii::$app->request->post('token');
            $vkPostId = Yii::$app->request->post('vk-post-id');

            if ($token !== Yii::$app->params["dn-vk-token-for-my-base"]) {
                return false;
            }

            $vkPostId = Helper::cleanData($vkPostId);
            if ($model = Vkposts::findOne($vkPostId)) {
                $model->watched = 1;
                $model->save();
                return true;
            }
        }
        return false;
    }

    public function actionTest()
    {
        return true;
    }
}
