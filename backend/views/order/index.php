<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            [
                'attribute' => 'created',
                'value' => function($data) {
                    /* @var $data \backend\models\Order */
                    return Yii::$app->formatter->asDatetime($data->created) . '<br> Москва ' . $data->createdMoscowTimeZone(). '<br> Таиланд ' . $data->createdThaiTimeZone();
                },
                'format' => 'html',
                'contentOptions' =>['class' => 'small-text'],
            ],

            [
                'attribute' => 'user_id',
                'value' => function($data) {
                    /* @var $data \backend\models\Order */
                    return '<a href="' . Url::to(['/user/view/'.$data->user_id]) . '">' . $data->user->email . "</a>"
                        . "<br>id: $data->user_id"
                        . "<br>point: " . $data->user->point
                        . "<br>parent: " . $data->user->getParentEmail();
                },
                'label' => 'email',
                'contentOptions' =>['class' => 'small-text'],
                'format' => 'html',
            ],
            'status',
            [
                'attribute' => 'date_paid',
                'value' => function($data) {
                    if ($data->date_paid) {
                        /* @var $data \backend\models\Order */
                        return Yii::$app->formatter->asDatetime($data->date_paid) . '<br> Москва ' . $data->datePaidMoscowTimeZone(). '<br> Таиланд ' . $data->datePaidThaiTimeZone();
                    } else {
                        return null;
                    }
                },
                'format' => 'html',
                'contentOptions' =>['class' => 'small-text'],
            ],
            [
                'attribute' => 'method',
                'value' => function($data) {
                    return $data->getNameMethod();
                }
            ],
            'amount',
            'operation_number',
            'info',
            'yandex_wallet',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
