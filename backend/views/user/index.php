<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            'point',
            'host_id',
            'status',
            'created_at:datetime',

            [
                    'label' => 'lastLogins',
                    'value' => function($data) {
                        if ($data->getLastLoginInfo() != []) {
                            $result = '';
                            /* @var $lastlogin backend\models\LoginInfo */
                            foreach ($data->getLastLoginInfo() as $lastlogin ) {
                                $result .= '<p>' . Yii::$app->formatter->asDatetime($lastlogin['datetime_login']) . ' ' . $lastlogin['ip'] . '</p>';
                            }
                            return $result;
                        } return null;
                    },
                    'format' => 'html',
            ],

            //'updated_at:datetime',
            'parent_user',
            'export_money',

            [
                'label' => 'add',
                'value' => function($data) {
                    return "<a href='". Url::to(['user/handler-add-points', 'id' => $data->id]) ."'><span class='glyphicon glyphicon-plus'></span></a>";
                },
                'format' => 'html',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
