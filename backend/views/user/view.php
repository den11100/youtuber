<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $dataProviderTasks yii\data\ActiveDataProvider  */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="row">

        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    //'auth_key',
                    //'password_hash',
                    //'password_reset_token',
                    'email:email',
                    'point',
                    'host_id',
                    'status',
                    'created_at:dateTime',
                    //'updated_at:dateTime',
                    'parent_user',
                    'export_money',
                ],
            ]) ?>
        </div>

        <div class="col-md-6">
            <?php foreach ($model->loginInfoList as $loginInfo): ?>
                <p>
                    <?php Yii::$app->formatter->timeZone = 'UTC' ?>
                    <span class="label label-info"><?= Yii::$app->formatter->asDatetime($loginInfo->datetime_login)?></span>
                    <?php Yii::$app->formatter->timeZone = 'Europe/Moscow' ?>
                    <span class="label label-warning">Москва: <?= Yii::$app->formatter->asDatetime($loginInfo->datetime_login) ?></span>
                    <?php Yii::$app->formatter->timeZone = 'Asia/Bangkok' ?>
                    <span class="label label-success">Таиланд: <?= Yii::$app->formatter->asDatetime($loginInfo->datetime_login) ?></span>
                    ip: <?= $loginInfo->ip ?>
                </p>
            <?php endforeach; ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

            <?= GridView::widget([
                'dataProvider' => $dataProviderTasks,
                'columns' => [

                    'id',
                    'created:datetime',
                    'user.point',
                    'url:url',
                    'description',
                    'second',
                    'budget',
                    'price',
                    'quantity',
                    'order_quantity',
                    //'type',
                    'status',
                    'rang',

                    //['class' => 'yii\grid\ActionColumn'],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'urlCreator' => function ($action, $model, $key, $index) {
                            return Url::to(['task/'.$action, 'id' => $model->id]);
                        }
                    ],
                ],
            ]); ?>

        </div>
    </div>

</div>
