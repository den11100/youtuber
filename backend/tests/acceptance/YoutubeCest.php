<?php namespace backend\tests\acceptance;

use backend\models\Vkposts;
use backend\tests\AcceptanceTester;
use Yii;
use yii\helpers\Url;
use yii\httpclient\Client;

class YoutubeCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->maximizeWindow();
    }

    // tests
    public function runTest(AcceptanceTester $I)
    {
        self::loginToSite($I);

        $vkPosts = Vkposts::find()->where(['watched' => 0])->asArray()->orderBy(['id' => SORT_ASC])->all();

        foreach ($vkPosts as $vkPost) {

            $url = $vkPost['link_video'];
            $time = 120 + rand(1,5);
            $result = self::doTask($I, $url, $time, $vkPost['vk_id_post'], $vkPost['vk_id_wall']);

            if ($result) {
                Vkposts::sendScreen($vkPost);
            }

            self::markWatchedVideo(Yii::$app->params['dn-vk-token-for-my-base'], $vkPost['id']);
        }
    }

    /**
     * @param $I \backend\tests\AcceptanceTester
     */
    private static function doTask($I, $url, $time, $vkIdMessage, $vkIdWall)
    {
        $I->wait(1);

        $I->amOnUrl($url);

        $I->wait(2);

        $resultCheck = self::checkVideoForUnavailable($url);

        if ($resultCheck) {

            $I->wait($time);

            $I->click(['css' => '.ytd-menu-renderer:nth-child(1) > .yt-simple-endpoint > #button > #button']);

            $I->wait(1);

            if (self::checkSubscribe($I)) {
                $I->click(['css' => 'ytd-subscribe-button-renderer > paper-button.style-scope.ytd-subscribe-button-renderer']);
            }

            self::makeComment($I);

            $I->wait(1);

            $I->makeScreenshot($vkIdMessage . "_" . $vkIdWall);

            $I->wait(1);

            return true;

        }

        return false;
    }

    /**
     * @param $I \backend\tests\AcceptanceTester
     */
    private static function loginToSite($I)
    {
        $I->amOnUrl('https://accounts.google.com/signin/v2/identifier?hl=ru&service=youtube&uilel=3&flowName=GlifWebSignIn&flowEntry=ServiceLogin');

        $I->wait(2);

        if ($I->grabTextFrom('h1 content') === "Вход") {

            $I->fillField('input[type=email]',Yii::$app->params['youtube-login']);

            $I->wait(1);

            $I->click(['css' => '#identifierNext']);

        }

        $I->wait(1);

        $I->click(['css' => 'input[type=password]']);

        $I->fillField('input[type=password]',Yii::$app->params['youtube-pass']);

        $I->wait(1);

        $I->click(['css' => '#passwordNext']);

        $I->wait(2);

        $I->amOnUrl('https://youtube.com');

        $I->wait(1);
    }

    /**
     * @param $I \backend\tests\AcceptanceTester
     */
    private static function makeComment($I)
    {
        $I->wait(1);

        $I->scrollTo('body',0 ,500);

        $I->wait(4);

        if ($I->grabTextFrom('#placeholder-area yt-formatted-string') === "Add a public comment...") {

            $I->click(['css' => '#placeholder-area']);

            $I->wait(1);

            $I->fillField('yt-formatted-string#contenteditable-textarea', Vkposts::getRandomYoutubeComment());

            $I->wait(1);

            $I->click(['css' => '#submit-button #text']);

            $I->wait(3);

        }
    }

    /**
     * @@param $I \backend\tests\AcceptanceTester
     * @return bool
     */
    private static function checkSubscribe($I)
    {
        $string = $I->grabTextFrom('yt-formatted-string.style-scope.ytd-subscribe-button-renderer');

        if (strpos($string, "SUBSCRIBED") === false) {
            return true;
        }
        return false;
    }

    /**
     * @param string $token
     * @param int $id
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    private static function markWatchedVideo($token, $id)
    {
        $url = Yii::$app->params['backend-site-url'] . '/vk/mark-watched-video';

        $data = ['token' => $token, 'vk-post-id' => $id];

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl($url)
            ->setData($data)
            ->send();
        return true;
    }

    /**
     * @param string $url
     * @return bool
     */
    private static function checkVideoForUnavailable($url)
    {
        $content = file_get_contents($url);
        preg_match('|<meta property="og:title" content="(.*?)">|', $content, $title);
        if ($title === []) {
            return false;
        }
        return true;
    }
}
