<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $created
 * @property int $user_id
 * @property string $url
 * @property string $description
 * @property int $second
 * @property int $budget
 * @property int $price
 * @property int $quantity
 * @property int $order_quantity
 * @property int $type
 * @property int $status
 * @property int $rang
 *
 * @property User $user
 */
class Task extends \common\models\Task
{

}
