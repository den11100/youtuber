<?php
namespace backend\models;

use Yii;

/**
 * User model
 *
 * @property integer $id
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property integer $point
 * @property integer $host_id
 * @property integer $parent_user
 * @property integer $export_money
 */
class User extends \common\models\User
{

}
