<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "vkposts".
 *
 * @property int $id
 * @property string $vk_id_post
 * @property string $vk_id_user
 * @property string $vk_id_wall
 * @property string $text
 * @property int $date
 * @property string $platform
 * @property string $link_video
 * @property int $duration_video
 * @property int $watched
 */
class Vkposts extends \common\models\Vkposts
{

}
