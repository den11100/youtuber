<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $created
 * @property int $user_id
 * @property int $status
 * @property string $date_paid
 * @property int $method
 * @property int $amount
 * @property string $operation_number
 * @property string $info
 * @property string $yandex_wallet
 *
 * @property User $user
 */
class Order extends \common\models\Order
{

}
