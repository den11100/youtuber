<?php
namespace backend\models;

use common\models\User;
use Yii;
use yii\web\ForbiddenHttpException;

/**
 * Login form
 */
class LoginForm extends \common\models\LoginForm
{
    private $_user;

    /**
     * Logs in a user using the provided email and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function loginAdmin()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getAdminUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        
        return false;
    }

    /**
     * Finds user by [[email]]
     *
     * @return mixed
     */
    protected function getAdminUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByemail($this->email);
        }

        if (in_array($this->_user->id, Yii::$app->params['userAdmins'])) {
            return $this->_user;
        } else {
            throw new ForbiddenHttpException('Access denied');
        }

        return $this->_user;
    }

}
