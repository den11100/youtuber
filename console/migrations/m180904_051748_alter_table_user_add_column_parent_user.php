<?php

use yii\db\Migration;

/**
 * Class m180904_051748_alter_table_user_add_column_parent_user
 */
class m180904_051748_alter_table_user_add_column_parent_user extends Migration
{
    public static $tableName = "{{%user}}";

    public function safeUp()
    {
        $this->addColumn(self::$tableName, 'parent_user', $this->integer()->defaultValue(null));
        $this->addColumn(self::$tableName, 'export_money', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::$tableName, 'parent_user');
        $this->dropColumn(self::$tableName, 'export_money');
    }
}
