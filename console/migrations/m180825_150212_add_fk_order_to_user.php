<?php

use yii\db\Migration;

/**
 * Class m180825_150212_add_fk_order_to_user
 */
class m180825_150212_add_fk_order_to_user extends Migration
{
    public $tableName = '{{%order}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_order_to_user',
            $this->tableName,
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_order_to_user', $this->tableName);
    }
}
