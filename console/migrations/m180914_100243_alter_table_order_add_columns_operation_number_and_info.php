<?php

use yii\db\Migration;

/**
 * Class m180914_100243_alter_table_order_add_columns_operation_number_and_info
 */
class m180914_100243_alter_table_order_add_columns_operation_number_and_info extends Migration
{
    public static $tableName = "{{%order}}";

    public function safeUp()
    {
        $this->addColumn(self::$tableName, 'operation_number', $this->string(255)->defaultValue(null));
        $this->addColumn(self::$tableName, 'info', $this->string(255)->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::$tableName, 'info');
        $this->dropColumn(self::$tableName, 'operation_number');
    }
}
