<?php

use yii\db\Migration;

/**
 * Class m180802_034844_add_fk_video_to_user
 */
class m180802_034844_add_fk_task_to_user extends Migration
{
    public $tableName = '{{%task}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_task_to_user',
            $this->tableName,
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_task_to_user', $this->tableName);
    }

}
