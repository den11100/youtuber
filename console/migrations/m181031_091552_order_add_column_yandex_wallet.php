<?php

use yii\db\Migration;

/**
 * Class m181031_091552_order_add_column_yandex_wallet
 */
class m181031_091552_order_add_column_yandex_wallet extends Migration
{
    public static $tableName = "{{%order}}";

    public function safeUp()
    {
        $this->addColumn(self::$tableName, 'yandex_wallet', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::$tableName, 'yandex_wallet');
    }
}
