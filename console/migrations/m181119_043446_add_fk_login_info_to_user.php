<?php

use yii\db\Migration;

/**
 * Class m181119_043446_add_fk_login_info_to_user
 */
class m181119_043446_add_fk_login_info_to_user extends Migration
{
    public $tableName = '{{%login_info}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_login_info_to_user',
            $this->tableName,
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_login_info_to_user', $this->tableName);
    }
}
