<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m180825_145459_create_order_table extends Migration
{
    public static $tableName = '{{%order}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(self::$tableName, [
            'id' => $this->primaryKey(),
            'created' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),
            'user_id' => $this->integer(11)->notNull(),
            'status' => $this->integer(11)->notNull(),
            'date_paid' => $this->dateTime(),
            'method' => $this->integer(11)->notNull(),
            'amount' => $this->integer(11)->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::$tableName);
    }
}
