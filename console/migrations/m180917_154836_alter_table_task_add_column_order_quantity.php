<?php

use yii\db\Migration;

/**
 * Class m180917_154836_alter_table_task_add_column_order_quantity
 */
class m180917_154836_alter_table_task_add_column_order_quantity extends Migration
{
    public static $tableName = "{{%task}}";

    public function safeUp()
    {
        $this->addColumn(self::$tableName, 'order_quantity', $this->integer(11));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::$tableName, 'order_quantity');
    }
}
