<?php

use yii\db\Migration;

/**
 * Handles the creation of table `video`.
 */
class m180802_032521_create_task_table extends Migration
{
    public static $tableName = '{{%task}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::$tableName, [
            'id' => $this->primaryKey(),
            'created' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),
            'user_id' => $this->integer(11)->notNull(),
            'url' => $this->string(255),
            'description' => $this->string(255),
            'second' => $this->integer(11),
            'budget' => $this->integer(11),
            'price' => $this->integer(11),
            'quantity' => $this->integer(11),
            'type' => $this->integer(11),
            'status' => $this->integer(11),
            'rang' => $this->integer(11),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::$tableName);
    }
}
