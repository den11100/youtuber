<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vkposts}}`.
 */
class m190201_053349_create_vkposts_table extends Migration
{
    public static $tableName = '{{%vkposts}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(self::$tableName, [
            'id' => $this->primaryKey(),
            'vk_id_post' => $this->string()->notNull(),
            'vk_id_user' => $this->string()->notNull(),
            'vk_id_wall' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),
            'date' => $this->integer()->notNull(),
            'platform' => $this->string(),
            'link_video' => $this->string(),
            'duration_video' => $this->integer(),
            'watched' => $this->integer()->defaultValue(0),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::$tableName);
    }
}
