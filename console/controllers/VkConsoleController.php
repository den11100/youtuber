<?php
namespace console\controllers;

use backend\models\Vkposts;
use VK\Client\VKApiClient;
use Yii;

/**
 * Clean controller
 */
class VkConsoleController extends \yii\console\Controller
{
    public function actionGrabMessage()
    {
        $vk = new VKApiClient();

        $listIdVkGroups = Yii::$app->params['vk_walls_id'];

        foreach ($listIdVkGroups as $wallId) {

            $posts = $vk->wall()->get(Yii::$app->params['vk_access_token'], [
                'owner_id' => $wallId, //-34281931, -108106884, -100492128
                'count' => 100,
                'filter' => 'others'
            ]);

            if (isset($posts['count']) && $posts['count'] === 0 ) {
                die('no-posts');
            }

            $result = Vkposts::savePosts($posts);

            sleep(3);
        }
    }
}
